
package grafos;

public class Grafos {

    
    public static Graph getCities(){
        Nodo Xpu = new Nodo("Xpujil");
        Nodo tena = new Nodo("Tenabo");
        Nodo can = new Nodo("Candelaria");
        Nodo cham = new Nodo("Champoton");
        Nodo cc = new Nodo("cuidad del carmen");
        Nodo esc = new Nodo("Escarcega");
        Nodo hec = new Nodo("Hecelchakan");
        Nodo hop = new Nodo("Hopelchen");
        

        
        
        Xpu.addEdge(new Edge(Xpu , tena, 341));
        Xpu.addEdge(new Edge(Xpu , can, 219));
        Xpu.addEdge(new Edge(Xpu , cham, 236));
        Xpu.addEdge(new Edge(Xpu , cc, 316));
        Xpu.addEdge(new Edge(Xpu, esc, 153));
        Xpu.addEdge(new Edge(Xpu , hec, 364));
        Xpu.addEdge(new Edge(Xpu , hop, 182));
 
        Graph graph = new Graph();
        graph.addNodo(Xpu);
        graph.addNodo(tena);
        graph.addNodo(can);
        graph.addNodo(cham);
        graph.addNodo(cc);
        graph.addNodo(esc);
        graph.addNodo(hec);
        graph.addNodo(hop);

        return graph;

 
    }
            
            
    public static void main(String[] args) {
        
        Graph graph = getCities();
        System.out.println(graph);
    }
    
}
